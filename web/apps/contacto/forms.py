from django.forms import ModelForm
from django import forms
from apps.contacto.models import Contacto
from apps.webmail.tasks import send_mail
from captcha.fields import CaptchaField


class ContactoForm(ModelForm):
    captcha = CaptchaField()

    class Meta:
        model = Contacto
        fields = ['nombre', 'telefono', 'email', 'tema', 'cuerpo', "captcha"]

    def send_usuario_email(self, *args, **kwargs):
        """
        kwargs keys => {from to subject body, template_info}
        template_info => {nivel_nombre nombre_completo fecha_inicio}
        """
        template = 'contacto/email_contacto.dj.html'
        opts = {
            'template_path':
            template,
            'template_info':
            dict(data_persona={
                key: value
                for key, value in kwargs.items() if key in self.fields
            }),
            'debug':
            True
        }
        kwargs.update(opts)
        fields = kwargs.get('fields')
        if fields:
            del kwargs['fields']
        try:
            send_mail.delay(fields, template, kwargs, debug=True)
        except Exception as e:
            raise e

    def send_admin_email(self, *args, **kwargs):
        """
        kwargs keys => {from to subject body, template_info}
        template_info => {data_persona}
        """
        template = 'contacto/email_admin_contacto.dj.html'
        opts = {
            'template_path':
            template,
            'template_info':
            dict(data_persona={
                key: value
                for key, value in kwargs.items() if key in self.fields
            }),
            'debug':
            True
        }
        kwargs.update(opts)
        fields = kwargs.get('fields')
        if fields:
            del kwargs['fields']
        try:
            send_mail.delay(fields, template, kwargs, debug=True)
        except Exception as e:
            raise e
