from django.db import models

# Create your models here.


class Contacto(models.Model):
    nombre = models.CharField(max_length=100)
    telefono = models.CharField(max_length=50)
    email = models.EmailField(max_length=354)
    tema = models.CharField(max_length=300)
    cuerpo = models.TextField()
    fecha_envio = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = "contacto"
        verbose_name = "Contacto"
        verbose_name_plural = "Contactos"
        ordering = ('fecha_envio', 'nombre', 'email', )

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return "%s -> %s :: %s" % (self.nombre, self.tema, self.fecha_envio)
