from celery.result import AsyncResult
from django.http import JsonResponse
from django.shortcuts import render
from .forms import ContactoForm
from django.views.generic import FormView
from django.views.generic import TemplateView
from django.conf import settings
from apps.home.views import menu_nav

# Create your views here.

ADMIN_EMAILS = getattr(settings, "ADMIN_EMAILS", None)
EMAIL_HOST_USER = getattr(settings, "EMAIL_HOST_USER", None)


class ContactoView(FormView):
    form_class = ContactoForm
    template_name = 'contacto/contacto.dj.html'

    def get_success_url(self):
        return reverse_lazy("contacto:exitoso")

    def get_context_data(self, *args, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        kwargs["menu_nav"] = menu_nav()
        return kwargs

    def post(self, request, *args, **kwargs):
        """
        Post modification
        """
        field_names = dict(
            zip(('tema', 'cuerpo', 'de', 'para'),
                ('subject', 'body', 'from', 'to')))
        form = ContactoForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            data = {
                'from': EMAIL_HOST_USER,
                'to': [cleaned_data.get('email', 'dpineda@uchile.cl')]
            }
            fields = {
                value: cleaned_data.get(key)
                for key, value in field_names.items()
            }
            cleaned_data.update(fields)
            cleaned_data.update(data)
            form.send_usuario_email(**cleaned_data)
            cleaned_data.update({'to': ADMIN_EMAILS})
            form.send_admin_email(**cleaned_data)
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


def contacto_sent_view(request, *args, **kwargs):
    return render(request, 'contacto/exitoso.dj.html', context=kwargs)


def status_task_contacto(request, task_id):
    data = {}
    if request.is_ajax and request.method == "GET":
        task = AsyncResult(task_id)
        status = False
        success = False
        value = 200
        if task.status == "SUCCESS":
            status = task.result.get("status")
            success = True
            value = 400
        data = {'task_id': task_id, 'status': status, "success": success}
        return JsonResponse(data, status=value)
    else:
        return JsonResponse(data, status=200)
