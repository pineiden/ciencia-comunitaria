from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

if settings.ENGINE == 'sqlite3':
    from jsonfield import JSONField
elif settings.ENGINE in {'postgis', 'postgresql'}:
    from django.db.models import JSONField
# Create your models here.


class Search(models.Model):
    busqueda = models.CharField(max_length=300)
    resultados = JSONField(null=True)
    fecha = models.DateTimeField(auto_now=True)
    usuario = models.ForeignKey(User,
                                related_name='searches',
                                on_delete=models.SET_NULL,
                                blank=True,
                                null=True)

    class Meta:
        app_label = "home"
        verbose_name = "Search"
        verbose_name_plural = "Searches"
        ordering = (
            'fecha',
            'busqueda',
            'usuario',
        )

    def __repr__(self):
        return f"Search({self.busqueda},{self.usuario},{self.fecha})"

    def __str__(self):
        return f"{self.busqueda} -> {self.usuario} :: {self.fecha}"
