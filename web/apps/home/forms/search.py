from apps.home.models import Search
from django import forms
from typing import Dict, Type, Text


class SearchForm(forms.ModelForm):
    """
    Cada búsqueda puede ser sencilla

    Objeto: Texto | (campo1, campo2, campo3)

    """
    __TARGETS: Dict[Text, Type] = {}
    __default: Text = ""

    class Meta:
        model = Search
        fields = ["busqueda", "usuario"]
        widgets = {'usuario': forms.HiddenInput()}
