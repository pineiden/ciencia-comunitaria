from django.views.generic import TemplateView
from apps.home.views import menu_nav

from .menu import app_menu


class AppView(TemplateView):
    template_name = 'proyecto/app.dj.html'
    app_active = "Proyecto"

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "app_menu": app_menu,
            "app_active": self.app_active,
            "menu": True,
        })
        return kwargs
