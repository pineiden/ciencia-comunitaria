from ..models import (Medicion, VarianteMedicion)
from django.views.generic import (
    CreateView, DeleteView,
    UpdateView, ListView,
    DetailView)
from .menu import app_menu
from apps.home.views import menu_nav
from extra_views import (CreateWithInlinesView,
                         UpdateWithInlinesView,
                         InlineFormSetFactory)


class BaseView:
    app_active = 'medicion'

    def context_data(self, *args, **kwargs):
        kwargs.update({
            'menu_nav': menu_nav,
            "app_menu": app_menu,
            "app_active": self.app_active,
            "menu": True,
        })
        return kwargs


class MedicionListView(ListView, BaseView):
    model = VarianteMedicion
    template_name = 'proyecto/medicion/list.dj.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update(self.context_data())
        return kwargs


class MedicionParametroInline(InlineFormSetFactory):
    model = Medicion
    fields = ["proyecto", "parametro"]
    factory_kwargs = {"extra": 1, "can_delete": False}


class MedicionCreateView(CreateWithInlinesView, BaseView):
    model = VarianteMedicion
    inlines = [MedicionParametroInline, ]
    fields = ["posicion", "icono", "rango", "tramos"]
    template_name = 'proyecto/medicion/create.dj.html'

    """
    TODO: Unir el campo 'medicion' al de MedicionParametro
    POST -> :) reescribir
    """

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update(self.context_data())
        kwargs["action"] = "Crear"
        return kwargs


class MedicionUpdateView(UpdateWithInlinesView, BaseView):
    model = VarianteMedicion
    inlines = [MedicionParametroInline, ]
    fields = ["posicion", "icono", "rango", "tramos"]
    template_name = 'proyecto/medicion/update.dj.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update(self.context_data())
        kwargs["action"] = "Actualizar"
        return kwargs


class MedicionDetailView(DetailView, BaseView):
    model = VarianteMedicion
    template_name = 'proyecto/medicion/detail.dj.html'
    pk_url_kwarg = 'id'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update(self.context_data())
        return kwargs


class MedicionDeleteView(DeleteView):
    model = VarianteMedicion
    template_name = 'proyecto/medicion/template.dj.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update(self.context_data())
        return kwargs
