from apps.proyecto.models import Clasificacion
from django.views.generic import (
    CreateView, DeleteView,
    UpdateView, ListView,
    DetailView)
from .menu import app_menu
from apps.home.views import menu_nav
from apps.proyecto.forms.clasificacion import ClasificacionForm
from pprint import pprint


class BaseView:
    app_active = 'clasificacion'

    def context_data(self, *args, **kwargs):
        kwargs.update({
            'menu_nav': menu_nav,
            "app_menu": app_menu,
            "app_active": self.app_active,
            "menu": True,
        })
        return kwargs


class ClasificacionListView(ListView, BaseView):
    model = Clasificacion
    template_name = 'proyecto/clasificacion/list.dj.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["action"] = "Crear"
        kwargs.update(self.context_data())
        return kwargs


class ClasificacionDetailView(DetailView, BaseView):
    model = Clasificacion
    template_name = 'proyecto/clasificacion/detail.dj.html'
    slug_field = 'slug_tipo'
    slug_url_kwarg = 'nombre'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["action"] = "Detallar"
        kwargs.update(self.context_data())
        return kwargs


class ClasificacionCreateView(CreateView, BaseView):
    model = Clasificacion
    template_name = 'proyecto/clasificacion/create.dj.html'
    form_class = ClasificacionForm

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["action"] = "Listar"
        kwargs.update(self.context_data())
        return kwargs


class ClasificacionUpdateView(UpdateView, BaseView):
    model = Clasificacion
    template_name = 'proyecto/clasificacion/update.dj.html'
    form_class = ClasificacionForm

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["action"] = "Actualizar"
        return kwargs


class ClasificacionDeleteView(DeleteView, BaseView):
    model = Clasificacion
    template_name = 'proyecto/clasificacion/delete.dj.html'
