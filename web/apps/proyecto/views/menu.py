from files.functions.links import LinkData


def app_menu():
    menu = {
        "Proyecto": [
            LinkData("Listar",
                     "proyecto:list",
                     model="proyecto",
                     perms=["view"]),
            LinkData("Crear",
                     "proyecto:create",
                     model="proyecto",
                     perms=["add"]),
        ],
        "Clasificación": [
            LinkData("Listar",
                     "proyecto:list_clasificacion",
                     model="clasificacion",
                     perms=["view"]),
            LinkData("Crear",
                     "proyecto:create_clasificacion",
                     model="clasificacion",
                     perms=["add"]),
        ],
        "Medición": [
            LinkData("Listar",
                     "proyecto:list_medicion",
                     model="medicion",
                     perms=["view"]),
            LinkData("Crear",
                     "proyecto:create_medicion",
                     model="medicion",
                     perms=["add"]),
        ],
    }
    return menu
