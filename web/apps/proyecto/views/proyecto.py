from apps.proyecto.models import Proyecto
from django.views.generic import (
    CreateView, DeleteView,
    UpdateView, ListView,
    DetailView)
from .menu import app_menu
from apps.home.views import menu_nav
from apps.proyecto.forms.proyecto import ProyectoForm
from pprint import pprint


class BaseView:
    app_active = 'proyecto'

    def context_data(self, *args, **kwargs):
        kwargs.update({
            'menu_nav': menu_nav,
            "app_menu": app_menu,
            "app_active": self.app_active,
            "menu": True,
        })
        return kwargs


class ProyectoListView(ListView, BaseView):
    model = Proyecto
    template_name = 'proyecto/main/list.dj.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update(self.context_data())
        return kwargs


class ProyectoDetailView(DetailView, BaseView):
    model = Proyecto
    template_name = 'proyecto/main/detail.dj.html'
    slug_field = 'slug_nombre'
    slug_url_kwarg = 'nombre'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update(self.context_data())
        return kwargs


class ProyectoCreateView(CreateView, BaseView):
    model = Proyecto
    template_name = 'proyecto/main/create.dj.html'
    form_class = ProyectoForm

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["action"] = "Crear"
        kwargs.update(self.context_data())
        return kwargs


class ProyectoUpdateView(UpdateView, BaseView):
    model = Proyecto
    template_name = 'proyecto/main/update.dj.html'
    form_class = ProyectoForm
    slug_field = 'slug_nombre'
    slug_url_kwarg = 'nombre'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update(self.context_data())
        kwargs["action"] = "Actualizar"
        return kwargs


class ProyectoDeleteView(DeleteView, BaseView):
    model = Proyecto
    template_name = 'proyecto/main/delete.dj.html'
