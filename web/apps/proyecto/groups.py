from groups_manager.models import (Group, Member, GroupEntity, GroupMember,
                                   GroupMemberRole)


class GrupoProyecto(Group):
    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        if not self.group_type:
            self.group_type = GroupType.objects.get_or_create(
                label='Proyecto')[0]
        super().save(*args, **kwargs)

    def get_members_dict(self, subgroups=True):
        members = {}
        for group in self.sub_grupos():
            members[group.name] = group.get_members()
        return members

    def get_sub_grupos(self, member):
        sub_groups = self.sub_grupos()
        member_groups = []
        for sb in sub_groups:
            qs = sb.group_membership.filter(member=member)
            if qs:
                member_groups.append(sb)
        return member_groups

    def sub_grupos(self, names=["Creador", "Responsable", "Colaborador"]):
        return list(self.sub_groups_manager_group_set.filter(name__in=names))

    def get_roles(self):
        group_members = self.group_membership.filter(group=self)
        roles = GroupMemberRole.objects.none()
        for gm in group_members:
            roles |= gm.roles.all()
        return roles.distinct()

    def get_creadores(self):
        """
        Creator group
        """
        return list(self.sub_groups_manager_group_set.filter(name="Creador"))

    def member_creadores(self):
        creadores = self.get_creadores()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)

    def get_responsables(self):
        return list(
            self.sub_groups_manager_group_set.filter(name="Responsable"))

    def member_responsables(self):
        creadores = self.get_responsables()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)

    def get_creador_y_responsables(self):
        return list(
            self.sub_groups_manager_group_set.filter(
                name__in=["Creador", "Responsable"]))

    def member_creador_y_responsables(self):
        creadores = self.get_creador_y_responsables()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)

    def get_estudiantes(self):
        return list(
            self.sub_groups_manager_group_set.filter(name="Colaborador"))

    def member_estudiantes(self):
        creadores = self.get_estudiantes()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)
