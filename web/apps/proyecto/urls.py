from django.urls import path
from apps.proyecto.views.proyecto import (ProyectoListView, ProyectoDetailView,
                                          ProyectoCreateView, ProyectoUpdateView,
                                          ProyectoDeleteView)
from apps.proyecto.views.clasificacion import (ClasificacionListView, ClasificacionDetailView,
                                               ClasificacionCreateView, ClasificacionUpdateView,
                                               ClasificacionDeleteView)

from apps.proyecto.views.medicion import (MedicionListView, MedicionDetailView,
                                          MedicionCreateView, MedicionUpdateView,
                                          MedicionDeleteView)
from apps.proyecto.views import (AppView)

app_name = 'proyecto'

urlpatterns = [
    path("app/", AppView.as_view(), name="application"),
    path("listar/", ProyectoListView.as_view(), name="list"),
    path("crear/", ProyectoCreateView.as_view(), name="create"),
    path("modificar/<slug:nombre>/", ProyectoUpdateView.as_view(), name="update"),
    path("ver/<slug:nombre>/", ProyectoDetailView.as_view(), name="detail"),
    path("borrar/<slug:nombre>/", ProyectoDeleteView.as_view(), name="delete"),
    path("clasificacion/listar/",
         ClasificacionListView.as_view(),
         name="list_clasificacion"),
    path("clasificacion/crear/",
         ClasificacionCreateView.as_view(),
         name="create_clasificacion"),
    path("clasificacion/modificar/<int:id>/",
         ClasificacionUpdateView.as_view(),
         name="update_clasificacion"),
    path("clasificacion/ver/<slug:nombre>/",
         ClasificacionDetailView.as_view(),
         name="detail_clasificacion"),
    path("clasificacion/borrar/<int:id>/",
         ClasificacionDeleteView.as_view(),
         name="delete_clasificacion"),
    path("medicion/listar/",
         MedicionListView.as_view(),
         name="list_medicion"),
    path("medicion/crear/",
         MedicionCreateView.as_view(),
         name="create_medicion"),
    path("medicion/modificar/<int:id>/",
         MedicionUpdateView.as_view(),
         name="update_medicion"),
    path("medicion/ver/id=<int:id>/",
         MedicionDetailView.as_view(),
         name="detail_medicion"),
    path("medicion/borrar/<int:id>/",
         MedicionDeleteView.as_view(),
         name="delete_medicion"),

]
