from apps.proyecto.models import Clasificacion
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _


class ClasificacionForm(ModelForm):
    class Meta:
        model = Clasificacion
        fields = ["tipo", "descripcion"]
        labels = {
            "tipo": _("Tipo"),
            "descripcion": _("Descripción")
        }
