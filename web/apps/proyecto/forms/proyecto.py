from apps.proyecto.models import Proyecto
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _


class ProyectoForm(ModelForm):
    class Meta:
        model = Proyecto
        fields = [
            "nombre",
            "clasificacion",
            "descripcion",
            "indefinido",
            "fecha_inicio",
            "fecha_final",
        ]
        labels = {
            "clasificacion": _("Clasificación"),
            "descripcion": _("Descripción")
        }
