from django.db import models
from groups_manager.models import Group, GroupType
from django.contrib.auth.models import User
from django.dispatch import receiver
from groups_manager.models import group_save, group_delete
from django.db.models.signals import post_save, post_delete
from autoslug import AutoSlugField
from apps.localizacion.models import Lugar, Territorio
from apps.icono.models import Icono
from apps.parametro.models import Parametro
from django.contrib.postgres.fields import ArrayField

from groups_manager.models import (Group, Member, GroupEntity, GroupMember,
                                   GroupMemberRole)

from django.utils.text import slugify
from django.urls import reverse
from martor.models import MartorField
import json


class GrupoProyecto(Group):
    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        if not self.group_type:
            self.group_type = GroupType.objects.get_or_create(
                label='Proyecto')[0]
        super().save(*args, **kwargs)

    def get_members_dict(self, subgroups=True):
        members = {}
        for group in self.sub_grupos():
            members[group.name] = group.get_members()
        return members

    def get_sub_grupos(self, member):
        sub_groups = self.sub_grupos()
        member_groups = []
        for sb in sub_groups:
            qs = sb.group_membership.filter(member=member)
            if qs:
                member_groups.append(sb)
        return member_groups

    def sub_grupos(self, names=["Creador", "Responsable", "Colaborador"]):
        return list(self.sub_groups_manager_group_set.filter(name__in=names))

    def get_roles(self):
        group_members = self.group_membership.filter(group=self)
        roles = GroupMemberRole.objects.none()
        for gm in group_members:
            roles |= gm.roles.all()
        return roles.distinct()

    def get_creadores(self):
        """
        Creator group
        """
        return list(self.sub_groups_manager_group_set.filter(name="Creador"))

    def member_creadores(self):
        creadores = self.get_creadores()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)

    def get_responsables(self):
        return list(
            self.sub_groups_manager_group_set.filter(name="Responsable"))

    def member_responsables(self):
        creadores = self.get_responsables()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)

    def get_creador_y_responsables(self):
        return list(
            self.sub_groups_manager_group_set.filter(
                name__in=["Creador", "Responsable"]))

    def member_creador_y_responsables(self):
        creadores = self.get_creador_y_responsables()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)

    def get_estudiantes(self):
        return list(
            self.sub_groups_manager_group_set.filter(name="Colaborador"))

    def member_estudiantes(self):
        creadores = self.get_estudiantes()
        members = set()
        for group in creadores:
            group_members = group.group_membership.all()
            for gmember in group_members:
                members.add(gmember.member)
        return set(members)


class Clasificacion(models.Model):
    tipo = models.CharField(max_length=150, unique=True)
    slug_tipo = models.SlugField(default=None, blank=True)
    descripcion = models.TextField(blank=True)

    def get_absolute_url(self):
        url = reverse('proyecto:detail_clasificacion',
                      kwargs={'slug': self.slug_tipo})
        return url

    def save(self, *args, **kwargs):
        self.slug_tipo = slugify(self.tipo)
        # TODO: check if exists before save
        instance = super().save(*args, **kwargs)
        return instance

    @property
    def Clasificacion(self):
        return self.tipo

    class Meta:
        app_label = "proyecto"
        verbose_name = "Clasificación"
        verbose_name_plural = "Clasificaciones"
        ordering = ("tipo", )

    def __repr__(self):
        return json.dumps(self.__dict__)

    def __str__(self):
        return self.tipo


class ProyectoManager(models.Manager):
    def get_by_natural_key(self, slug_nombre):
        return self.get(slug_nombre=slug_nombre)


class Proyecto(models.Model):
    nombre = models.CharField(default='Nombre de Proyecto',
                              max_length=300,
                              unique=True)
    slug_nombre = AutoSlugField(populate_from='nombre')
    group = models.OneToOneField(GrupoProyecto,
                                 related_name='proyecto',
                                 on_delete=models.SET_NULL,
                                 blank=True,
                                 null=True)
    clasificacion = models.ManyToManyField(Clasificacion,
                                           related_name="proyectos")
    descripcion = MartorField()
    fecha_creacion = models.DateField(auto_now=True)
    indefinido = models.BooleanField(default=False)
    fecha_inicio = models.DateField()
    fecha_final = models.DateField(blank=True, null=True)
    usuario = models.ForeignKey(User,
                                related_name="proyectos",
                                on_delete=models.SET_NULL,
                                null=True,
                                blank=True)

    objects = ProyectoManager()

    def natural_key(self):
        return (self.slug_nombre, )

    def get_absolute_url(self):
        return reverse('proyecto:detail',
                       kwargs={
                           'nombre': self.slug_nombre,
                       })

    def __str__(self):
        return f"{self.nombre}@{self.usuario}"

    def __repr__(self):
        return f"Proyecto({self.nombre}@{self.usuario})"

    @property
    def slug(self):
        return self.slug_nombre

    class Meta:
        app_label = "proyecto"
        verbose_name = "Proyecto"
        verbose_name_plural = "Proyectos"
        ordering = ("fecha_creacion", "nombre")
        permissions = (("create_project", "Crear Proyecto"), )


class GeoInfo(models.Model):
    proyecto = models.OneToOneField(Proyecto,
                                    related_name='geoinfos',
                                    on_delete=models.SET_NULL,
                                    null=True,
                                    blank=True)
    lugar = models.ForeignKey(Lugar,
                              related_name='geoinfos',
                              on_delete=models.SET_NULL,
                              null=True,
                              blank=True)
    territorio = models.ForeignKey(Territorio,
                                   related_name='territorios',
                                   on_delete=models.SET_NULL,
                                   null=True,
                                   blank=True)
    sectores = models.ManyToManyField(Territorio,
                                      related_name='sectores',
                                      blank=True)

    class Meta:
        app_label = "proyecto"
        verbose_name = "GeoInfo"
        verbose_name_plural = "GeoInfos"
        ordering = ("proyecto", )


class VarianteMedicion(models.Model):
    posicion = models.PositiveIntegerField(default=0)
    icono = models.ForeignKey(Icono,
                              related_name='variantes_mediciones',
                              on_delete=models.SET_NULL,
                              null=True,
                              blank=True)
    rango = ArrayField(models.FloatField(), size=2)
    tramos = models.PositiveIntegerField()

    class Meta:
        app_label = "proyecto"
        verbose_name = "Variante Medición"
        verbose_name_plural = "Variante Mediciones"
        ordering = ("posicion", "tramos")

    def natural_key(self):
        return (self.slug_nombre, )

    def get_absolute_url(self):
        return reverse('proyecto:detail_medicion',
                       kwargs={
                           'id': self.id,
                       })


class Medicion(models.Model):
    proyecto = models.ForeignKey(Proyecto,
                                 related_name='mediciones',
                                 on_delete=models.SET_NULL,
                                 null=True,
                                 blank=True)
    parametro = models.ForeignKey(Parametro,
                                  related_name='mediciones',
                                  on_delete=models.SET_NULL,
                                  null=True,
                                  blank=True)
    variantes = models.ForeignKey(VarianteMedicion,
                                  related_name='mediciones',
                                  on_delete=models.SET_NULL,
                                  null=True,
                                  blank=True)

    class Meta:
        app_label = "proyecto"
        verbose_name = "Medición"
        verbose_name_plural = "Mediciones"
        ordering = ("proyecto", )
