from django.views.generic import (
    CreateView, DeleteView,
    UpdateView, ListView,
    DetailView)


class ProyectoListView(ListView):


class ProyectoDetailView(DetailView):


class ProyectoCreateView(CreateView):


class ProyectoUpdateView(UpdateView):


class ProyectoDeleteView(DeleteView):
