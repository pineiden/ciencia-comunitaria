from django.contrib import admin
from .models import Lugar, Territorio
# Register your models here.
admin.site.register(Lugar)
admin.site.register(Territorio)
