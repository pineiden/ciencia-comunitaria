from .models import Localizacion, Comuna
from django.forms import ModelForm
from django import forms
from dal_select2_queryset_sequence.widgets import QuerySetSequenceSelect2
from dal import autocomplete


class NewLocalizacionForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(NewLocalizacionForm, self).__init__(*args,**kwargs)
        self.fields['comuna'].widget.attrs = {
            'class' : 'comuna-select'
        }

    class Meta:
        model = Localizacion
        fields = ['ubicacion','numero','comuna']
        widgets = {
            'comuna' : autocomplete.ModelSelect2( url = 'comuna-autocomplete'),
        }
