from django.contrib.gis.db import models
# Create your models here.
from django_countries.fields import CountryField
# database regiones, provincias y comunas Chile
# https://github.com/knxroot/bdcut-cl
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
# https://docs.djangoproject.com/es/1.9/ref/contrib/contenttypes/
from django.urls import reverse
from .geo_methods import geolocale, point_field
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import Point
from django.contrib.gis.db.models import (PointField, PolygonField,
                                          GeometryCollectionField)


class Lugar(models.Model):
    """
    :model:`localizacion.Localizacion` contiene las ubicaciones o direcciones
    particulares, referenciadas a la comuna y al punto geográfico.

    Set un punto geográfico
    """
    ubicacion = models.CharField(max_length=500,
                                 help_text="Nombre de calle",
                                 verbose_name='Ubicación')
    pais = models.CharField(max_length=100,
                            help_text="Ingresa nombre de país",
                            verbose_name='País')
    geo_point = PointField(blank=True)
    content_type = models.ForeignKey(ContentType,
                                     on_delete=models.CASCADE,
                                     null=True,
                                     blank=True,
                                     editable=False)
    object_id = models.PositiveIntegerField(null=True,
                                            blank=True,
                                            editable=False)
    content_object = GenericForeignKey('content_type', 'object_id')

    @property
    def direccion(self):
        direccion = f"{self.ubicacion}, {self.pais}"
        return direccion

    class Meta:
        app_label = 'localizacion'
        verbose_name = 'Lugar'
        verbose_name_plural = 'Lugares'

    def __str__(self):
        return self.direccion

    def json(self):
        return {"pais":self.pais,
                "ubicacion": self.ubicacion,
                "x": self.geo_point.x,
                "y": self.geo_point.y}
    # Definir metodo save para generar el punto geográfico
    # def save(self):

    @property
    def point(self):
        return dict(zip(("x", "y"), (self.geo_point.x, self.geo_point.y)))

    def get_absolute_url(self):
        return reverse('ubicacion', kwargs={'pk': self.pk})


class Territorio(models.Model):
    """
    Define un territorio, es decir un polígono geográfico
    """
    codigo = models.CharField(max_length=20)
    territorio = PolygonField()
