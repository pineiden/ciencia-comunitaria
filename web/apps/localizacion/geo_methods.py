import geocoder

from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import Point


def geolocale(direccion):
    geocode = geocoder.osm(direccion)
    print("On geolocale", geocode)
    lat_lon = geocode.latlng
    print("latlng:", lat_lon)
    return lat_lon


def point_field(latlon):
    print("Latlon", latlon)
    if latlon:
        latlon.reverse()
        geo_point = GEOSGeometry(Point(latlon), srid=4326)
        return geo_point
    else:
        latlon = (0, 0)
        return GEOSGeometry(Point(latlon), srid=4326)
