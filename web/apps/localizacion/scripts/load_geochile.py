import os
from apps.localizacion.models import Region, Comuna
import csv, codecs

def load_places():
	dir = os.path.dirname(os.path.abspath(__file__))
	print(dir)
	comunas_path = dir+'/comunas.txt'
	regiones_path = dir+'/regiones.txt'
	comunas = codecs.open( comunas_path, 'r' , encoding='utf-8')
	regiones = codecs.open( regiones_path, 'r', encoding='utf-8')

	for line in regiones:
		print(line, end='')
		region_tupla = line.split(',')
		r = Region()
		print(str(len(region_tupla[0])))
		r.nombre = region_tupla[0]
		r.numero_decimal = str(region_tupla[1].replace("\r\n", ""))
		print(r.nombre)
		r.save()
		del r

	for line in comunas:
		print(line, end='')
		comuna_tupla = line.split(',')
		print(comuna_tupla)
		nombre_comuna = comuna_tupla[0]
		c = Comuna()
		c.nombre = nombre_comuna
		c.region = Region.objects.filter( numero_decimal = comuna_tupla[1].replace("\r\n", "") )[0]
		c.save()
		del c

