from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.measure import D
from files.functions.numeric import is_numeric

def create_point(x:str, y:str)->GEOSGeometry:
    if is_numeric(x)[1] and is_numeric(y)[1]:
        point_string = f"Point({x} {y})"
        point = GEOSGeometry(point_string, srid=4326)
        return point
