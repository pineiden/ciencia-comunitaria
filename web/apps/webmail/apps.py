from django.apps import AppConfig

class WebmailConfig(AppConfig):
    name = 'apps.webmail'
