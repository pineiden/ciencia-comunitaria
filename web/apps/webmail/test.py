from datetime import datetime
from .send import webmail_send
from .tasks import send_mail

def prueba_webmail():
    template = 'webmail/prueba.dj.html'
    template_info = { "nombre":"David",
               "fecha": datetime.now(),
               "curso":"Sapiensia sabelotoda",
               "direccion":"Los 77 guerreros, Ulalia",
               "dia":"Lunes, 7 octubre"}
    mail_fields = {
        "subject":"Inscripción a curso aceptada",
        "body":"",
        "from":"dpineda@csn.uchile.cl",
        "to":["dpineda@uchile.cl"]
    }
    result = webmail_send(mail_fields, template, template_info)
    print("Resultado->", result)
    return result


def prueba_sendmail_task():
    """
    With celery manager
    """
    template = 'webmail/prueba.dj.html'
    template_info = { "nombre":"David",
               "fecha": datetime.now(),
               "curso":"Sapiensia sabelotoda",
               "direccion":"Los 77 guerreros, Ulalia",
               "dia":"Lunes, 7 octubre"}
    mail_fields = {
        "subject":"Inscripción a curso aceptada",
        "body":"",
        "from":"dpineda@csn.uchile.cl",
        "to":["dpineda@uchile.cl"]
    }
    print("sending to: ",mail_fields)
    result = send_mail.delay(mail_fields, template, template_info, True)
    print("Resultado->", result, type(result))
    return result
