from django.contrib import admin
from .models import Admin


# Register your models here.
@admin.register(Admin)
class AdminGroupAdmin(admin.ModelAdmin):
    list_display = ["id", "slug_name"]
