from groups_manager.models import (Group, Member, GroupEntity, GroupMember,
                                   GroupMemberRole, GroupType)


def get_member(user):
    profile = user.profile
    member, created = Member.objects.get_or_create(django_user=user)
    if created:
        member.username = user.username
        member.first_name = user.first_name
        member.last_name = f"{profile.father_last_name} {profile.mother_last_name}"
        member.email = user.email
        member.save()
    return member


def my_cursos(member, managers=["Creador", "Responsable", "Administración"]):
    """
    Dado un miembro a algún grupo, extrae los cursos asociados
    dada la lista de 'GroupType' en string.
    Verifica que los grupos tengan un grupo 'parent'
    Extraer los proyectos asociados
    Si se necesita obtener los curso de 'Estudiante'
    cambiar a managers=["Estudiante"]
    """
    group_memberships = member.group_membership.all()
    GrupoCurso = GroupType.objects.get_or_create(label='Curso')[0]
    gt_managers = list(
        map(lambda name: GroupType.objects.get_or_create(label=name)[0],
            managers))
    select_groups = list(
        filter(lambda gm: gm.group.group_type in gt_managers,
               group_memberships))
    parent_groups = set(filter(lambda g: g.group.parent, select_groups))
    grupos_cursos = set(
        filter(lambda g: g.group.parent.group_type == GrupoCurso,
               parent_groups))
    cursos = list({g.group.parent.curso for g in grupos_cursos})
    return cursos


def my_proyectos(member,
                 managers=[
                     "Creador", "Responsable", "Profesor", "Ayudante",
                     "Administración"
                 ]):
    """
    Dado un miembro a algún grupo, extrae los proyectos asociados
    dada la lista de 'GroupType' en string.
    Verifica que los grupos tengan un grupo 'parent'
    Extraer los proyectos asociados
    Si se necesita obtener los proyectos de 'Estudiante'
    cambiar a managers=["Estudiante"]
    """
    group_memberships = member.group_membership.all()
    GrupoProyecto = GroupType.objects.get_or_create(label='Proyecto')[0]
    gt_managers = list(
        map(lambda name: GroupType.objects.get_or_create(label=name)[0],
            managers))
    select_groups = list(
        filter(lambda gm: gm.group.group_type in gt_managers,
               group_memberships))
    parent_groups = set(filter(lambda g: g.group.parent, select_groups))
    grupos_proyectos = set(
        filter(lambda g: g.group.parent.group_type == GrupoProyecto,
               parent_groups))
    proyectos = list({g.group.parent.proyecto for g in grupos_proyectos})
    return proyectos
