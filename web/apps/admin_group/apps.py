from django.apps import AppConfig


class AdminGroupConfig(AppConfig):
    name = 'admin_group'
