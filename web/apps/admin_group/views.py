from .forms import RemoveMemberGroupForm
from .models import RemoveMemberFromGroup
from apps.proyecto.models import GrupoProyecto
from groups_manager.models import Group, Member, GroupMemberRole
from .forms import GroupMemberForm
from django.views.generic import (CreateView, DeleteView, DetailView, FormView,
                                  UpdateView)
from django.views.generic import ListView
from django.shortcuts import render
from django.views.generic import TemplateView
from files.functions.links import LinkData
from .models import AdminGroup
from .forms import AdminGroupForm
from apps.accounts.models import Profile
from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect


def app_menu():
    menu = {
        "Listar": [
            LinkData("grupos", "admin_group:list-groups"),
            LinkData("usuarios", "admin_group:list-users")
        ],
        "Editar": [
            LinkData("crear", "admin_group:create-group"),
        ],
        "Asociar": [LinkData("Usuario a Grupo", "admin_group:asociate")]
    }
    return menu


class ApplicationView(TemplateView):
    template_name = 'admin_group/app.dj.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context["menu"] = app_menu()
        return context


# Create your views here.
class CreateGroupView(FormView):
    template_name = 'admin_group/create_group.dj.html'
    form_class = AdminGroupForm

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context["menu"] = app_menu()
        return context

    def get_success_url(self):
        return reverse("admin_group:application")


class EditGroupView(UpdateView):
    template_name = 'admin_group/edit_group.dj.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context["menu"] = app_menu()
        return context


class DeleteGroupView(DeleteView):
    template_name = 'admin_group/delete_group.dj.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context["menu"] = app_menu()
        return context


class DeleteGroupOkView(TemplateView):
    template_name = 'admin_group/delete_group_ok.dj.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context["menu"] = app_menu()
        return context


class GroupView(DetailView):
    template_name = 'admin_group/detail_group.dj.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context["menu"] = app_menu()
        return context


class GroupsListView(ListView):
    template_name = 'admin_group/list_groups.dj.html'
    model = AdminGroup

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context["menu"] = app_menu()
        return context


class UsersListView(ListView):
    template_name = 'admin_group/list_users.dj.html'
    model = Profile

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context["menu"] = app_menu()
        return context


class EditUserWithGroupView(FormView):
    template_name = 'admin_group/edit_user_with_group.dj.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context["menu"] = app_menu()
        return context


def get_roles(group_qs, grupo, member):
    roles = GroupMemberRole.objects.none()
    for group_member in group_qs:
        if group_member.group == grupo and group_member.member == member:
            roles |= group_member.roles.all()
    return roles.distinct()


class AddMemberToGroupProjectView(FormView, DetailView):
    #template_name = 'admin_group/add_member_to_group.dj.html'
    template_name = 'admin_group/group_add_member.dj.html'
    form_class = GroupMemberForm
    model = GrupoProyecto

    def get_success_url(self):
        proyecto = self.object.proyecto
        url = proyecto.get_absolute_url()
        print("URL result", url)
        return url

    def get_form_kwargs(self):
        data = super().get_form_kwargs()
        if hasattr(self, "object"):
            if self.object:
                data["instance"] = self.object
        return data

    def form_valid(self, form):
        print("Cleaned data", form.cleaned_data)
        # get member
        member_id = form.cleaned_data.get("member")
        group_roles = form.cleaned_data.get("roles")
        group = self.object
        member = Member.objects.get(pk=member_id)
        roles = list(map(lambda x: Group.objects.get(pk=x), group_roles))
        # just add member
        if member not in group.get_members():
            for role in roles:
                role.add_member(member, roles=group.get_roles())
                role.save()
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        pk = kwargs.get('pk')
        self.object = GrupoProyecto.objects.get(pk=pk)
        form = self.get_form()
        return super().post(request, *args, **kwargs)


class EditMemberToGroupProjectView(FormView, DetailView):
    #template_name = 'admin_group/add_member_to_group.dj.html'
    template_name = 'admin_group/group_add_member.dj.html'
    form_class = GroupMemberForm
    model = GrupoProyecto

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data()
        context["menu"] = app_menu()
        context["member"] = self.member
        return context

    def get_initial(self):
        initial = super().get_initial()
        member_id = self.kwargs.get('member_id')
        project_id = self.kwargs.get('pk')
        member = Member.objects.get(pk=member_id)
        subgroups = self.object.sub_grupos()
        groupmembers = member.group_membership.filter(group__in=subgroups)
        initial["member"] = member.id
        self.member = member
        select_roles = [g.group.id for g in groupmembers]
        initial["roles"] = select_roles
        return initial

    def get_success_url(self):
        proyecto = self.object.proyecto
        url = proyecto.get_absolute_url()
        return url

    def get_form_kwargs(self):
        data = super().get_form_kwargs()
        if hasattr(self, "object"):
            if self.object:
                data["instance"] = self.object
        return data

    def form_valid(self, form):
        # get member
        member_id = form.cleaned_data.get("member")
        group_roles = form.cleaned_data.get("roles")
        group = self.object
        member = Member.objects.get(pk=member_id)
        roles = group.get_sub_grupos(member)
        roles_inicial = set(map(lambda g: g.id, roles))
        roles_seleccion = set(map(int, group_roles))
        # solo borrar los que tengan
        borrar = list(roles_inicial - roles_seleccion)
        # agregar
        agregar = list(roles_seleccion - roles_inicial)
        # just delete member:: group are subsgroups of...
        if borrar:
            borrar_groups = Group.objects.filter(pk__in=borrar)
            for del_group in borrar_groups:
                del_group.remove_member(member)
                del_group.save()
        if agregar:
            agregar_groups = Group.objects.filter(pk__in=agregar)
            for add_group in agregar_groups:
                gm = add_group.group_membership.all()
                roles = get_roles(gm, group, member)
                add_group.add_member(member, roles=roles)
                add_group.save()
        #
        return super().form_valid(form)

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        pk = kwargs.get('pk')
        self.object = GrupoProyecto.objects.get(pk=pk)
        form = self.get_form()
        return super().post(request, *args, **kwargs)
