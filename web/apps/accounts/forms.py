import base64

from django.contrib.auth import forms as auth_forms
from django import forms
from captcha.fields import CaptchaField
from django.contrib.auth.models import User
from django.conf import settings
from django.urls import reverse, reverse_lazy
from django.forms.models import model_to_dict

from .tokens import account_activation_token

from apps.webmail.tasks import send_mail

SITE_NAME = getattr(settings, "SITE_NAME", None)
SITE_URL = getattr(settings, "SITE_URL", None)
"""
Ref:
https://docs.djangoproject.com/en/1.8/_modules/django/contrib/auth/forms/
"""


class RegistrarUsuarioForm(auth_forms.UserCreationForm):
    captcha = CaptchaField()
    first_name = forms.CharField(label="Nombre",
                                 max_length=100,
                                 help_text="Nombre")
    last_name = forms.CharField(label="Apellido",
                                max_length=100,
                                help_text="Apellido")
    acepta = forms.BooleanField()

    class Meta:
        model = User
        fields = ("first_name", "last_name", "username", "email", "password1",
                  "password2", "captcha")

    def __init__(self, no_captcha=False, initial={}, *args, **kwargs):
        self.no_captcha = no_captcha
        super().__init__(initial=initial, *args, **kwargs)
        self.fields['captcha'].widget.attrs.update(
            {'id': 'captcha-field-registro'})
        self.fields['email'].required = True
        self.fields["captcha"].required = self.no_captcha
        if self.no_captcha:
            self.fields.pop('captcha')

    def send_usuario_email(self, *args, **kwargs):
        """
        kwargs keys => {from to subject body, template_info}
        template_info => {nivel_nombre nombre_completo fecha_inicio}
        """
        template = 'registration/activar_user.dj.html'
        user = kwargs.get('user')
        fields = kwargs.get('fields')
        uid = base64.b64encode(str(user.pk).encode()).decode()
        token = account_activation_token.make_token(user)
        if fields:
            del kwargs['fields']
        kwargs.update({
            'uid': uid,
            'token': token,
        })
        kwargs["user"] = model_to_dict(kwargs["user"])
        try:
            action = send_mail.delay(fields, template, kwargs, debug=True)
            return action
        except Exception as e:
            print("Error al enviar email...", e)
            raise e

    def send_admin_email(self, *args, **kwargs):
        """
        kwargs keys => {from to subject body, template_info}
        template_info => {data_persona}
        """
        template = 'registration/activar_user_admin.dj.html'
        user = kwargs.get('user')
        fields = kwargs.get('fields')
        kwargs["user"] = model_to_dict(kwargs["user"])
        if fields:
            del kwargs['fields']
        try:
            email = send_mail.delay(fields, template, kwargs, debug=True)
            return email
        except Exception as e:
            raise e

    def save(self, commit=False, *args, **kwargs):
        user = super().save(commit=False)
        user.is_active = False
        user.save()
        return user
