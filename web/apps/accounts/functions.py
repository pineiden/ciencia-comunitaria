from .models import ProfilePlaces
from apps.localizacion.models import Lugar
from pprint import pprint
from apps.localizacion.functions import create_point
from django.contrib.auth.models import User

def off_place(place):
    place.activo = False
    place.save()

def create_user_place(user: User,
                      all_data: dict,
                      radius: float = 100.0)->ProfilePlaces:
    """
    Buscar Lugar por zona en torno al punto
    """
    places = ProfilePlaces.objects.filter(profile=user.profile, activo=True)
    for p in places:
        off_place(p)
    # buscar por lugar
    data = all_data.get('value')
    # buscar por cercania
    point = create_point(data.get('x'), data.get('y'))
    qs = Lugar.objects.filter(geo_point__distance_lte=(point, radius))
    # crear
    lugar = None
    lugar_data = {
        "geo_point": point,
        "ubicacion": data.get('ciudad'),
        "pais": data.get('pais')
    }
    if qs:
        lugar = qs.first()
    else:
        # alguna correccion a futuro
        lugar, created = Lugar.objects.get_or_create(**lugar_data)
    new_place, created = ProfilePlaces.objects.get_or_create(
        profile=user.profile, ubicacion=lugar)
    new_place.activo = True
    new_place.save()
    return new_place.json()
