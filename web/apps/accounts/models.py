from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from phonenumber_field.modelfields import PhoneNumberField
from sorl.thumbnail import ImageField
from apps.localizacion.models import Lugar


class Profile(models.Model):
    user = models.OneToOneField(User,
                                on_delete=models.CASCADE,
                                related_name="profile")
    first_name = models.CharField(max_length=100, blank=True)
    father_last_name = models.CharField(max_length=100, blank=True)
    mother_last_name = models.CharField(max_length=100, blank=True)
    email = models.EmailField(max_length=150)
    phonenumber = PhoneNumberField(blank=True)
    bio = models.TextField()
    signup_confirmation = models.BooleanField(default=False)

    def __str__(self):
        nombre_total=f"""{self.first_name} {self.father_last_name} {self.mother_last_name}"""
        return nombre_total if nombre_total.strip() else self.user.username

    @property
    def username(self):
        return self.user.username

    @property
    def profile_image(self):
        print(self.principal)
        for prof in self.principal.all():
            if prof.principal:
                return prof.image

    @property
    def ubicacion(self):
        lugar = self.profiles_places.filter(activo=True).last()
        return lugar

    class Meta:
        app_label = "accounts"
        verbose_name = "Perfil"
        verbose_name_plural = "Perfiles"
        ordering = ("first_name", "father_last_name", "mother_last_name",
                    "email")


@receiver(post_save, sender=User)
def update_profile_signal(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance, email=instance.email)
    instance.profile.save()


def profile_images(instance, filename):
    return 'user_{0}/{1}'.format("perfiles", filename)


class ProfilePlaces(models.Model):
    profile = models.ForeignKey(Profile,
                                on_delete=models.SET_NULL,
                                related_name='profiles_places',
                                null=True)
    ubicacion = models.ForeignKey(Lugar,
                                  on_delete=models.SET_NULL,
                                  related_name='profiles_places',
                                  null=True)
    activo = models.BooleanField(default=True)
    registrado = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.profile} -> {self.ubicacion}"

    class Meta:
        app_label = "accounts"
        verbose_name = "Ubicación de Usuario"
        verbose_name_plural = "Ubicaciones de Usuario"
        ordering = ("-registrado", "activo", "ubicacion",
                    "profile")

    @property
    def point(self):
        return self.ubicacion.point

    def json(self):
        return {"username": self.profile.username,
                "ubicacion": self.ubicacion.json(),
                "activo": self.activo,
                "registrado": self.registrado}

# Install
# sudo apt-get install libjpeg62 libjpeg62-dev zlib1g-dev
# https://sorl-thumbnail.readthedocs.io/en/latest/requirements.html


class ProfileImage(models.Model):
    tag = models.CharField(max_length=100, default="imagen normal")
    image = ImageField(upload_to=profile_images)
    date_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.tag

    class Meta:
        app_label = "accounts"
        verbose_name = "Imagen de Perfil"
        verbose_name_plural = "Imagenes de Perfile"
        ordering = (
            "tag",
            "date_time",
        )


class PrincipalImageProfile(models.Model):
    profile = models.ForeignKey(Profile,
                                on_delete=models.CASCADE,
                                related_name='principal')

    image = models.ForeignKey(ProfileImage,
                              on_delete=models.CASCADE,
                              related_name="principal")
    date_time = models.DateTimeField(auto_now=True)
    principal = models.BooleanField(default=True)

    class Meta:
        app_label = "accounts"
        verbose_name = "Imagen Principal de Perfil"
        verbose_name_plural = "Imagenes Principal de Perfil"
        ordering = (
            "profile",
            "date_time",
        )
