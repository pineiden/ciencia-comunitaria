from django.conf.urls import url
from django.urls import path
from django.contrib.auth import views as auth_views
from .views import (AppAccountsView, RegistrarUsuario, activate,
                    activation_sent_view, status_task_signup, LoginView,
                    ProfileView, LogoutView, PasswordChangeView,
                    PasswordChangeDoneView, profile_update_field)
from django.urls import reverse, reverse_lazy

app_name = 'accounts'

urlpatterns = [
    path('', AppAccountsView.as_view(), name='application'),
    path('app', AppAccountsView.as_view(), name='application'),
    path('login/', LoginView.as_view(), name='login'),
    path(
        'logout',
        LogoutView.as_view(),  # not override template
        name='logout'),
    path('password_change',
         PasswordChangeView.as_view(
             template_name="registration/password_change.dj.html",
             extra_context={'menu': True}),
         name='password_change'),
    path('password_change/done',
         PasswordChangeDoneView.as_view(
             template_name="registration/password_change_done.dj.html",
             extra_context={'menu': True}),
         name='password_change_done'),
    path('password_reset',
         auth_views.PasswordResetView.as_view(
             template_name="registration/password_reset.dj.html",
             email_template_name="registration/password_reset_email.dj.html",
             html_email_template_name="registration/password_reset_email.dj.html",
             subject_template_name="registration/password_reset_subject.txt",
             success_url=reverse_lazy("accounts:password_reset_done"),
             extra_context={'menu': True}),
         name='password_reset'),
    path('password_reset/done',
         auth_views.PasswordResetDoneView.as_view(
             template_name="registration/password_reset_done.dj.html",
             extra_context={'menu': True}),
         name='password_reset_done'),
    path('password-reset/confirm/uidb64=<uidb64>/token=<token>',
         auth_views.PasswordResetConfirmView.as_view(
             template_name='registration/password_reset_confirm.dj.html',
             success_url=reverse_lazy('accounts:password_reset_complete'),
             extra_context={'menu': True}),
         name='password_reset_confirm'),
    path('password-reset/complete/',
         auth_views.PasswordResetCompleteView.as_view(
             template_name='registration/password_reset_complete.dj.html',
             extra_context={'menu': True}),
         name='password_reset_complete'),
    path("signup/", RegistrarUsuario.as_view(), name='signup'),
    path('activacion-enviada/task=<task_code>',
         activation_sent_view,
         name='activacion-enviada'),
    path('activar/uidb64=<uidb64>/token=<token>/', activate, name='activate'),
    path("ajax/status_task/task=<str:task_id>",
         status_task_signup,
         name="status_task"),
    path("perfil/", ProfileView.as_view(), name='profile'),
    path("perfil/usuario=<str:username>",
         ProfileView.as_view(),
         name='profile_user'),
    path("perfil/update_field/usuario=<str:username>",
         profile_update_field,
         name='profile_update_field'),
]
