import io
import json
import base64
from django.core.files.base import File
from django.core.files.base import ContentFile
from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import views as auth_views
from celery.result import AsyncResult
from django.http import JsonResponse
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic.edit import FormView
from django.views.generic import DetailView
from .tokens import account_activation_token
from django.http import Http404
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.urls import reverse, reverse_lazy
from .forms import RegistrarUsuarioForm
from django.conf import settings
from django.contrib.auth.models import User
from apps.home.views import menu_nav
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Profile, ProfileImage, PrincipalImageProfile
from pprint import pprint


ADMIN_EMAILS = getattr(settings, "ADMIN_EMAILS", [])
EMAIL_HOST_USER = getattr(settings, "DEFAULT_FROM_EMAIL", None)
SITE_URL = getattr(settings, "SITE_URL", "localhost")
SITE_NAME = getattr(settings, "SITE_NAME", "Curso de Programación")


# Create your views here.
class AppAccountsView(TemplateView):
    template_name = 'accounts/app.dj.html'


class RegistrarUsuario(FormView):
    form_class = RegistrarUsuarioForm
    template_name = 'registration/signup.dj.html'
    app_active = "Profile"

    def form_valid(self, form):
        fields = ('subject', 'body', 'from', 'to')
        user = form.save()
        user.refresh_from_db()
        acepta = form.cleaned_data.get('acepta')
        if acepta:
            user.profile.first_name = form.cleaned_data.get('first_name')
            user.profile.last_name = form.cleaned_data.get('last_name')
            email = form.cleaned_data.get('email')
            user.profile.email = email
            user.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            # construcción de data email
            user_fields = dict(
                zip(fields, (
                    f"Registro en web de {SITE_NAME}",
                    "Registro exitoso!",
                    EMAIL_HOST_USER,
                    [email],
                )))
            opts = {'user': user, 'fields': user_fields, 'domain': SITE_URL}
            user_email = form.send_usuario_email(**opts)
            admin_fields = dict(
                zip(fields, (
                    f"Registro de nuevo usuario en web {SITE_NAME}",
                    "Se ha registrado una nueva persona!",
                    EMAIL_HOST_USER,
                    ADMIN_EMAILS,
                )))
            admin_opts = {
                'user': user,
                'fields': admin_fields,
                'domain': SITE_URL
            }
            form.send_admin_email(**admin_opts)
            self.opts = {"task_code": getattr(user_email, "id")}
            return super().form_valid(form)
        else:
            return super().form_invalid(form)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        return reverse('accounts:activacion-enviada', kwargs=self.opts)

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "app_active": self.app_active,
            "menu": True
        })
        return kwargs


def activation_sent_view(request, *args, **kwargs):
    return render(request,
                  'registration/activate_sent.dj.html',
                  context=kwargs)


def activate(request, uidb64, token):
    try:
        uid = int(base64.b64decode(uidb64.encode()))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    # checking if the user exists, if the token is valid.
    if user and account_activation_token.check_token(user, token):
        # if valid set active true
        user.is_active = True
        # set signup_confirmation true
        user.profile.signup_confirmation = True
        user.save()
        login(request,
              user,
              backend='django.contrib.auth.backends.RemoteUserBackend')
        return redirect(reverse('home:index'))
    else:
        return render(request, 'registration/activacion_invalida.dj.html')


def status_task_signup(request, task_id):
    data = {}
    if request.is_ajax and request.method == "GET":
        task = AsyncResult(task_id)
        status = False
        success = False
        value = 200
        if task.status == "SUCCESS":
            status = task.result.get("status")
            success = True
            value = 400
        data = {'task_id': task_id, 'status': status, "success": success}
        return JsonResponse(data, status=value)
    else:
        return JsonResponse(data, status=200)


# Create your views here.
# accounts/login/ [name='login']
class LoginView(auth_views.LoginView):
    template_name = "registration/login.dj.html"
    app_active = "Profile"

    def get_success_url(self, *args, **kwargs):
        return reverse('accounts:profile_user',
                       kwargs={'username': self.request.user.username})

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update({
            'menu_nav': menu_nav,
            "app_active": self.app_active,
            "menu": True
        })
        return kwargs


class ProfileView(LoginRequiredMixin, DetailView):
    model = User
    template_name = 'accounts/profile.dj.html'
    slug_field = 'username'
    slug_url_kwarg = 'username'
    app_active = "Profile"

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        place = self.request.user.profile.ubicacion.point
        print("Lugar usuario", self.request.user)
        print("Place->", place)
        default_position = {"lat": -33.447487,
                            'lon': -70.673676}
        if place:
            default_position = place
        mode = "_".join(sorted(default_position.keys()))
        kwargs.update({
            'menu_nav': menu_nav,
            "app_active": self.app_active,
            "menu": True,
            "map_params": {
                "maps": {
                    "modal": "map_modal",
                    "page": "map"
                },
                "editable": {
                    "modal": True,
                    "page": False
                },
                "geometria": {
                    "modal": "point",
                    "page": "point"
                },
                "position": {
                    "modal": default_position,
                    "page": default_position,
                },
                "mode": {
                    "modal": mode,
                    "page": mode,
                }
            }
        })
        return kwargs


# accounts/logout/ [name='logout']
class LogoutView(auth_views.LogoutView):
    next_page = reverse_lazy('home:index')


# accounts/password_change/ [name='password_change']
class PasswordChangeView(LoginRequiredMixin, auth_views.PasswordChangeView):
    template_name = "registration/cambio_password.dj.html"
    success_url = reverse_lazy('accounts:password_change_done')


# accounts/password_change/done/ [name='password_change_done']
class PasswordChangeDoneView(LoginRequiredMixin,
                             auth_views.PasswordChangeDoneView):
    template_name = "registration/cambio_password_lista.dj.html"


"""
Ajax calls
"""

from .functions import create_user_place

@csrf_exempt
def profile_update_field(request, *args, **kwargs):
    user_fields = {
        "username",
        "email",
        "first_name",
        "last_name",
    }
    profile_fields = {
        "first_name", "father_last_name", "mother_last_name", "email",
        "phonenumber", "bio", "ubicacion"
    }
    image_profile_fields = {"tag", "image"}
    new = False
    if request.is_ajax():
        data_dict = json.loads(request.body)
        user = User.objects.get(username=kwargs.get("username"))
        profile = user.profile
        # if field name from data in setsockopt
        all_changes = {}
        imageprofile = None
        if any(map(lambda k: k in data_dict, image_profile_fields)):
            imageprofile, new = ProfileImage.objects.get_or_create(
                tag=data_dict.get("tag", {}).get("value"))
        for key, data in data_dict.items():
            field_name = data.get("field_name")
            value = data.get("value")
            changes = {
                "field_name": field_name,
                "user": False,
                "profile": False,
                "from_user": "",
                "from_profile": "",
                "to": value
            }
            if field_name in user_fields:
                changes["from_user"] = getattr(user, field_name)
                setattr(user, field_name, value)
                user.save()
                changes["user"] = True
            if field_name in profile_fields:
                if field_name == 'phonenumber':
                    changes["from_profile"] = field_name
                if field_name == 'ubicacion':
                    """
                    Crear un nuevo lugar para el perfiles
                    entregar como resultado.
                    """
                    changes["from_profile"] = create_user_place(user, data)
                else:
                    changes["from_profile"] = getattr(profile, field_name)
                if field_name != "ubicacion":
                    setattr(profile, field_name, value)
                    profile.save()
                changes["profile"] = True
            if field_name in image_profile_fields:
                changes["image_profile_" + field_name] = getattr(
                    imageprofile, field_name)
                if field_name == "image":
                    file_part = value.split("\\")[-1].split(".")
                    filename = slugify(file_part[0]) + "." + file_part[1]
                    blob = data.get("blob")
                    format_data, imgstr = blob.split(";base64,")
                    ext = format_data.split("/")[-1]
                    django_file = ContentFile(base64.b64decode(imgstr),
                                              name=filename + "." + ext)
                    imageprofile.image.save(filename, django_file)
                    changes["url"] = imageprofile.image.url
                else:
                    setattr(imageprofile, field_name, value)
                changes["image_profile_" + field_name] = True
            all_changes[key] = changes
            if new:
                imageprofile.save()
                before_principal = PrincipalImageProfile.objects.filter(
                    profile=user.profile)
                for before in before_principal:
                    before.principal = False
                    before.save()
                principal_image = PrincipalImageProfile(profile=user.profile,
                                                        image=imageprofile,
                                                        principal=True)
                principal_image.save()
        return JsonResponse(all_changes)
    return JsonResponse({})
