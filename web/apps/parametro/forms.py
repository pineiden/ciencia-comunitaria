from django.forms import ModelForm
from .models import Parametro
from django import forms


class ParametroForm(ModelForm):
    class Meta:
        model = Parametro
        fields = [
            "tipo",
            "nombre",
            "unidad_medida",
            "descripcion",
            "icono"
        ]
        widgets = {
            "tipo": forms.RadioSelect()
        }
