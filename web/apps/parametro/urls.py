from django.urls import path
from .views import (ParametroListView, ParametroDetailView,
                    ParametroCreateView, ParametroUpdateView,
                    ParametroDeleteView)

from .views import (AppView)

app_name = 'parametro'

urlpatterns = [
    path("app/", AppView.as_view(), name="application"),
    path("listar/", ParametroListView.as_view(), name="list"),
    path("crear/", ParametroCreateView.as_view(), name="create"),
    path("modificar/<int:id>/", ParametroUpdateView.as_view(), name="update"),
    path("ver/<slug:nombre>/", ParametroDetailView.as_view(), name="detail"),
    path("borrar/<int:id>/", ParametroDeleteView.as_view(), name="delete"),
]

"""


"""
