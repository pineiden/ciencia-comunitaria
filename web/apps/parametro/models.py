from django.db import models
from apps.icono.models import Icono
from autoslug import AutoSlugField
from django.urls import reverse


class Parametro(models.Model):
    DEFAULT = "SB"
    TIPOS_CHOICES = {
        (DEFAULT, "subjetiva"),
        ("OB", "objetiva"),
    }
    tipo = models.CharField(max_length=10,
                            choices=TIPOS_CHOICES,
                            default=DEFAULT)
    nombre = models.CharField(max_length=100)
    unidad_medida = models.CharField(max_length=10)
    slug = AutoSlugField(populate_from='nombre')
    descripcion = models.TextField()
    icono = models.ForeignKey(Icono,
                              related_name='parametros',
                              on_delete=models.SET_NULL,
                              null=True,
                              blank=True)

    class Meta:
        app_label = "parametro"
        verbose_name = "Parámetro"
        verbose_name_plural = "Parámetros"
        ordering = ("nombre", )

    def natural_key(self):
        return (self.slug_nombre, )

    def get_absolute_url(self):
        return reverse('parametro:detail',
                       kwargs={
                           'nombre': self.slug,
                       })

    def __str__(self):
        return f"{self.nombre}@{self.tipo}@{self.unidad_medida}"
