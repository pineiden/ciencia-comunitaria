from django.views.generic import (
    CreateView, DeleteView,
    UpdateView, ListView,
    DetailView, TemplateView)
from .forms import ParametroForm
from .models import Parametro
from apps.home.views import menu_nav
from django.http import HttpResponse, HttpResponseRedirect


from files.functions.links import LinkData


def app_menu():
    menu = {
        "Parámetro": [
            LinkData("Listar",
                     "parametro:list",
                     model="parametro",
                     perms=["view"]),
            LinkData("Crear",
                     "parametro:create",
                     model="parametro",
                     perms=["add"]),
        ],
    }
    return menu


class BaseView:
    app_active = 'parametro'

    def context_data(self, *args, **kwargs):
        kwargs.update({
            'menu_nav': menu_nav,
            "app_menu": app_menu,
            "app_active": self.app_active,
            "menu": True,
        })
        return kwargs


class AppView(TemplateView, BaseView):
    template_name = 'parametro/app.dj.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["action"] = "Crear"
        kwargs.update(self.context_data())
        return kwargs


class ParametroListView(ListView, BaseView):
    model = Parametro
    template_name = 'parametro/list.dj.html'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["action"] = "Crear"
        kwargs.update(self.context_data())
        return kwargs


class ParametroDetailView(DetailView, BaseView):
    model = Parametro
    template_name = 'parametro/detail.dj.html'
    slug_field = 'slug'
    slug_url_kwarg = 'nombre'

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs.update(self.context_data())
        return kwargs


class ParametroCreateView(CreateView, BaseView):
    model = Parametro
    template_name = 'parametro/create.dj.html'
    form_class = ParametroForm

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["action"] = "Crear"
        kwargs.update(self.context_data())
        return kwargs


class ParametroUpdateView(UpdateView, BaseView):
    model = Parametro
    template_name = 'parametro/update.dj.html'
    form_class = ParametroForm

    def get_context_data(self, *args, **kwargs):
        kwargs = super().get_context_data(**kwargs)
        kwargs["action"] = "Actualizar"
        return kwargs


class ParametroDeleteView(DeleteView, BaseView):
    model = Parametro
    template_name = 'parametro/delete.dj.html'
