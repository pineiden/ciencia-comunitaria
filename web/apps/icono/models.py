from django.db import models
from autoslug import AutoSlugField
from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator
from django.conf import settings

# Create your models here.
if settings.ENGINE == 'sqlite3':
    from jsonfield import JSONField
elif settings.ENGINE in {'postgis', 'postgresql'}:
    from django.db.models import JSONField


class Icono(models.Model):
    """
    Iconos para utilizar en las distintas mediciones.
    """
    nombre = models.CharField(max_length=50)
    slug = AutoSlugField(populate_from='nombre', unique=True)
    descripcion = models.TextField()
    archivo = models.FileField(null=True,
                               blank=True,
                               validators=[FileExtensionValidator(['svg'])])
    fecha = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = "icono"
        verbose_name = "Icono"
        verbose_name_plural = "Iconos"
        ordering = (
            'fecha',
            'nombre',
            'descripcion',
            'archivo',
        )

    def __repr__(self):
        return f"Icono({self.nombre}, {self.fecha})"

    def __str__(self):
        return f"{self.nombre} ->  {self.fecha}"


class PackIconos(models.Model):
    """
    TODO:
    Save file and unpack
    And load the files of json
    Then, uncompress and set the icons
    """
    fecha = models.DateTimeField(auto_now=True)
    archivo = models.FileField(
        null=True,
        blank=True,
        validators=[FileExtensionValidator([
            'tar.gz',
            'zip',
            "7zip",
        ])])
    asignacion = JSONField()
    descripcion = models.TextField()

    class Meta:
        app_label = "home"
        verbose_name = "Pack Iconos"
        verbose_name_plural = "Packs Iconos"
        ordering = (
            'fecha',
            'archivo',
        )

    def __repr__(self):
        return f"PackIconos({self.archivo},{self.fecha})"

    def __str__(self):
        return f"{self.fecha} -> {self.archivo.name}"
