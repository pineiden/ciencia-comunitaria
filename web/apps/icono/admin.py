from django.contrib import admin
from .models import Icono, PackIconos

admin.site.register(Icono)

admin.site.register(PackIconos)
