from django.shortcuts import render
from django.views.generic import (DetailView,
                                  ListView,
                                  CreateView,
                                  UpdateView,
                                  DeleteView,
                                  TemplateView)
from .models import (Clasificacion, Pagina)
from django.urls import reverse, reverse_lazy
from django.contrib.messages.views import SuccessMessageMixin

from apps.home.views import menu_nav
# Create your views here.


class VistaPagina(TemplateView):
    slug_field = 'slug_titulo'
    template_name = 'pagina/pagina.dj.html'

    def get_context_data(self, **kwargs):
        """
        Dado el nombre del proyecto en slug
        se consulta la información del proyecto
        para entregar la visualización relacionada a este mismo
        """
        kwargs = super().get_context_data(**kwargs)
        titulo_pagina = kwargs.get('slug_titulo')
        pagina_data = Pagina.objects.filter(
            slug_titulo=titulo_pagina).first()
        kwargs.update({'object': pagina_data})
        kwargs.update({'menu_nav': menu_nav()})
        return kwargs


class ListarPaginas(ListView):
    model = Pagina
    template_name = 'pagina/lista_paginas.dj.html'
    allow_empty = True
    paginate_by = 20


class CrearPagina(CreateView):
    model = Pagina
    template_name = 'pagina/edita_pagina.dj.html'
    slug_field = 'slug_titulo'

    def get_success_url(self):
        return reverse_lazy(
            'edicion_exitosa/',
            args=(self.object.id))


class EditaPagina(UpdateView):
    model = Pagina
    template_name = 'pagina/edita_pagina.dj.html'
    slug_field = 'slug_titulo'

    def get_success_url(self):
        return reverse_lazy(
            'edicion_exitosa/',
            args=(self.object.id))


class EliminaPagina(DeleteView):
    model = Pagina
    template_name = 'pagina/elimina_pagina.dj.html'
    slug_field = 'slug_titulo'

    def get_success_url(self):
        return reverse_lazy(
            'eliminacion_exitosa/',
            args=(self.object.id))


class EdicionPaginaExitosa(VistaPagina):
    template_name = 'pagina/edicion_pagina_exitosa.dj.html'


class EliminacionPaginaExitosa(TemplateView):
    template_name = 'pagina/eliminacion_pagina_exitosa.dj.html'
