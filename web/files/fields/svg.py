from django.db import models
from django.utils.translation import gettext as _
import magic
from filer.fields.file import FilerFileField
from filer.models.filemodels import File


class SVGFile(File):
    @classmethod
    def matches_file_type(cls, iname, ifile, request):
        filename_extensions = ['.svg', '.svgz']
        ext = os.path.splitext(iname)[1].lower()
        return ext in filename_extensions


class SVGField(FilerFileField):
    """
    This field allows only SVG files
    """
    description = _('SVGField for models, allow only *svg* files')
    default_model_class = SVGFile
