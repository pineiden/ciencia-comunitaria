from django.urls import reverse


class LinkData:
    css_id = ""
    css_class = ""

    def __init__(self, name, link, title=None, model="", *args, **kwargs):
        self.name = name.capitalize()
        largs = []
        lkwargs = {}
        opts = kwargs.get('opts')
        if opts:
            largs = opts.get('args', [])
            lkwargs = opts.get('kwargs', {})
        # que acciones se pueden hacer al entrar en enlace
        self.model = model
        self.perms = kwargs.get('perms', ["view"])
        self.go_to = kwargs.get("go_to")
        self.set_id(kwargs.get("css_id", ""))
        self.set_class(kwargs.get("css_class", ""))
        self.icon = kwargs.get('icon')
        self.link = reverse(link, args=largs, kwargs=lkwargs)
        if not title:
            self.title = name

    def __str__(self):
        return self.name

    def set_id(self, css_id):
        self.css_id = css_id

    def set_class(self, css_class):
        self.css_class = css_class

    def add_perm(self, perm):
        if isinstace(perm, str):
            self.perms.append(perm)

    def drop_perm(self, perm):
        if perm in self.perms:
            self.perms.remove(pem)

    @property
    def permissions(self):
        return [f"{perm}_{self.model}" for perm in self.perms]

    @property
    def action(self):
        return self.name.lower()

    @property
    def fa_icon(self):
        if self.icon:
            return f"<i class='fa fa-{self.icon}'></i>"
        return ""

    @property
    def go_to_html(self):
        if self.go_to:
            return f"#{self.go_to}"
        return ""

    @property
    def html(self):
        link = f'''<a href={self.link}{self.go_to_html}
         title={self.title}
         class="{self.css_class}"
         id="{self.css_id}">
         {self.fa_icon}{self.name}</a>'''
        return link

    def __repr__(self):
        return f"LinkData({self.name},{self.link})"
