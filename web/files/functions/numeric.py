def is_numeric(texto):
    if texto.isdigit():
        return int(texto), True
    elif all(filter(lambda e:e.isdigit(),texto.split("."))):
        return float(texto), True
    else:
        return texto, False
