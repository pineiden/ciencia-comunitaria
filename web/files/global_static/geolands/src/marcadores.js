import Point from 'ol/geom/Point';
import {Polygon} from 'ol/geom/Polygon';
import {Vector} from 'ol/layer/Vector';
import Feature from 'ol/Feature';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';

import {Circle as CircleStyle,
        Fill,   Icon,
        Stroke,   Style, } from 'ol/style';

/* Tooltip for markers */ //
import Overlay from 'ol/Overlay';
import Popup from 'ol-popup';
const fs=eval(require('graceful-fs'));

/*
Coordinate Transform
 */
import { fromLonLat } from 'ol/proj';
/*
Manipulacion del marcador
*/

// Añadir a layer

export function data2feature(dataList, tipo_coords='lat_lon', name_field='code') {
  const featuresMarkers = [];

    dataList.forEach((elem) => {
        console.log("Elemento", elem);
        let coordinate = [];
        if (tipo_coords==='lat_lon' || tipo_coords==='latitud_longitud'){
	          coordinate = fromLonLat(
                [elem.value.longitud?elem.value.longitud:elem.value.lon,
                 elem.value.latitud?elem.value.latitud:elem.value.lat]);
        }else if (tipo_coords==='x_y'){
	          coordinate = [parseFloat(elem.value["x"]),
                                parseFloat(elem.value["y"])];
        }
        let point = new Point(coordinate); 
       const feature = new Feature({
           type: elem.type?elem.type:"rtx",
         geometry: point,
         name: 'station' in elem ? elem[name_field] : 'stgo',
       });
        // const featProperties = feature.getProperties();}
        feature.control = true;
        feature.data = elem;
        featuresMarkers.push(feature);
  });
  return featuresMarkers;
}

import {loadMarkers, loadIcons, loadStyles} from './load_marker.js';

/*
Definición de function de estilos
*/
const markers = loadMarkers();
const icons = loadIcons(markers);
const styles = loadStyles(markers, icons);

export function styleFeature(feature) {
    return styles[feature.get('type')?feature.get('type'):"geoMarker"];
}

/*
Cargar marcadores y estilos en vectorlayer
*/

// features_markers=features_markers.concat([gnssMarker, svg_gnssMarker]);

export function features2source(featuresMarkers) {
    /* Esta función junta la llamada al vector del icono
       y los estilos
       (source, style)
    */
  const vectorLayer = new VectorLayer({
    source: new VectorSource({
      features: featuresMarkers,
    }),
    style: styleFeature,
  });
  return vectorLayer;
}

export function genMarker(data, coordenadas, campo){
    let features = data2feature(data, coordenadas, campo);
    let vector_source = features2source(features);
    return {vector_source, features};
}

/*

Tooltip factory

*/

/*
Show when mouse is hovering over the marker
 */

export function tooltipName(ids, title) {
  const html = 
    ```<div class='tooltip'>
                    <div class='identificador'>
    ${ids}
    </div>
                    <div class='name'>
    ${title},
    </div>
                </div>```;
  return html;
}

