const fs=eval(require('graceful-fs'));

import 'ol/ol.css';
import './estilos.css';

/*
Openlayer objects
*/
import {Map, View} from 'ol';
import {Tile} from 'ol/layer';
import TileLayer from 'ol/layer/Tile';
import {OSM} from 'ol/source';
import {fromLonLat} from 'ol/proj';
import {toLonLat} from 'ol/proj';
import {Draw, Modify, Snap} from 'ol/interaction';
import {Feature} from 'ol/Feature';
import {Point} from 'ol/geom/Point';
import {Polygon} from 'ol/geom/Polygon';
import {Vector} from 'ol/layer/Vector';


/*
OL Geocoder
*/
import 'ol-geocoder/dist/ol-geocoder.min.css';
const Geocoder=require('ol-geocoder');

import {of, fromEvent} from 'rxjs';
import { map, tap } from 'rxjs/operators';

import './estilos.sass';
import bulmaCollapsible from '@creativebulma/bulma-collapsible';
/*
Para seleccionar y mover marker
 */
import Select from 'ol/interaction/Select';
import {click, pointerMove} from 'ol/events/condition';

// update field
import {update_field} from '../../js/update_field.js';
import {close_modal} from '../../modals/functions.js';

/**/

const lugar_card = document.getElementById("collapsible-lugar");
if (lugar_card){
    const collapsibleLugar= new bulmaCollapsible(lugar_card);

function collapse_lugar(){
    lugar_card.bulmaCollapsible("collapse");
}
    ;
}

const posicion_card = document.getElementById("collapsible-posicion");
if (posicion_card){
    const collapsiblePosicion= new bulmaCollapsible(posicion_card);

    function collapse_posicion(){
        posicion_card.bulmaCollapsible("collapse");
    }
    ;
}

/**/

let select = null;
// selector al mover -> demarcar
let selectPointerMove = new Select({
    condition: pointerMove,
});
// al hacer click tomar y activar draggable
var selectClick = new Select({
  condition: click,
});

export function changeInteraction(){
    let value = 'pointermove';
    if(select!==null){
        ol_map.removeInteraction(select);
    }
    if (value === 'pointerMove'){
        select = selectPointerMove;
    }
    else if (value === 'click'){
        select = selectClick;
    }else{
        select = null;
    }

    if (select!==null){
        map.addInteraction(select);
        select.on('select', function(e){

        });
    }
}

/*

  Just One position

*/

function show_map(map_id, position, mode='lat_lon'){
    var position_;
    if (mode==='lat_lon'){
        let lon_lat = [position.lon, position.lat];
        position_=fromLonLat(lon_lat);
    } else if(mode==='x_y'){
        position_=[position.x, position.y];
    }

    let ol_map = new Map({
        target: map_id,
        layers:[
            new TileLayer({source: new OSM()})
        ],
        view: new View({
            center: position_,
            zoom:5
        }),
    });
    ol_map.render();
    return ol_map;
}

function load_geocoder(ol_map){
    /* Si mapa es editable, se activa el form*/
    /* the location form */
    let form = document.getElementById('form-lugar');
    let lugar_form = form.elements;

    let geocoder = new Geocoder('nominatim',{
        provider: 'osm',
        key: '__some_key__',
        lang: 'es-LA', //en-US, fr-FR
        placeholder: 'Buscar lugar ...',
        targetType: 'text-input',
        limit: 5,
        keepOpen: true
    });

    ol_map.addControl(geocoder);

    /* Capturar evento con RXJS */
    const select_location = fromEvent(geocoder, 'addresschosen');


    let info_stream = select_location.pipe(
        tap(event=>console.log(event)),
        map(event=>{
            let lon_lat = toLonLat(event.coordinate,);
            return {
                x: event.coordinate[0],
                y: event.coordinate[1],
                pais: event.address.details.country,
                ciudad:event.address.details.city,
                nombre: event.address.details.name,
                latitud: lon_lat[0],
                longitud: lon_lat[1],
            };
        }));

    info_stream.subscribe({
        next:  data => {
            Object.entries(data).forEach(([key, value])=>{
                if (lugar_form.hasOwnProperty(key)){
                    lugar_form[key].value = value;
                }
            });
        },
        error: err => console.log("error",err),
        complete: () => console.log("Selección completa")});


}

/*
Move marker
 */


export function load_maps(){
    if (!window.maps){
        window.maps={};
    }
    /*
      Paraetros entrega los siguientes json:
      - maps :: mapas a activar
      - editable :: mapas editable
      - geometria :: geometria habilitada (point, polygon)

      TODO:
      - seleccionar marcador y cargar esa data en el form
      - limpiar marcadores anteriores
    */
    let form = document.getElementById('form-lugar');
    let data=document.getElementById('map_params');
    let params = JSON.parse(data.textContent);
    /*
      Por cada ítem dentro de maps
    */
    let maps = params.maps;
    let modes = params.mode;
    let editable = params.editable;
    let position = params.position;
    Object.entries(maps).forEach(([key,value])=>{
        // key is destiniy,
        let ol_map = show_map(value, position[key], modes[key]);
        let coords = modes[key];
        let array_data = [{"value":position[key]}];
        window.maps[value]=ol_map;
        console.log(window.maps);
        if (position[key]){
            let gm = put_marker(value, array_data, coords);}
        if (editable[key]){
            load_geocoder(ol_map);
        }
    });
}


import {genMarker} from './marcadores.js';

function getFromForm(form){
    let field_names = ["ciudad", "nombre","pais", "latitud", "longitud",
                       "x", "y"];
    let values = {};
    field_names.forEach(name=>{
        values[name]=form.elements[name].value?form.elements[name].value:form.elements[name];
    });
    let fields = {
        name:"ubicacion",
        field_name:"ubicacion",
        type:"rtx",
        value:values};
    return fields;    
}

export function put_marker(destiny, array_data, coords='x_y', code='test'){
    let gm = genMarker(array_data, coords, code);
    let map = window.maps[destiny];
    let marker= gm.vector_source;
    map.addLayer(marker);
    return {map, gm};
}

export function take_position(idname, destiny){
    let form = document.getElementById(idname);
    console.log("Form data", form);
    if(form){
        let data = getFromForm(form);
        let array_data = [data];
        let map_gm=put_marker(destiny,array_data);
        let features=map_gm.gm.features;
        console.log(map,data, features);
        return {map, data, features};
    }
}



function flyTo(view, location, done) {
    var duration = 3000;
    var zoom = 8;
    var parts = 2;
    var called = false;
    function callback(complete) {
        --parts;
        if (called) {
            return;
        }
        if (parts === 0 || !complete) {
            called = true;
            done(complete);
        }
    }
    view.animate({
        center: location,
        duration: duration
    }, callback);
    view.animate({
        zoom: zoom - 4,
        duration: duration / 2
    }, {
        zoom: zoom,
        duration: duration / 2
    }, callback);
}


export function move_to(map, position){
    flyTo(map.getView(), position, function(){});
}

/*
from update_field.js
update_field(*)

from profile.js
close_modal(*)

 */
export function update_place(modal_id){
     let modal=document.getElementById("modal_"+modal_id);
     modal.classList.add("is-active");
     let modal_close=document.getElementById("modal_close_"+modal_id);
     let ol_map = window.maps["map_modal"];
     console.log("Update map", ol_map);
     ol_map.updateSize();
     modal_close.addEventListener("click", event=>{
         modal.classList.remove("is-active");
     });
     let btn_modal_close=document.getElementById("btn_modal_close_"+modal_id);
     btn_modal_close.addEventListener("click", event=>{
         modal.classList.remove("is-active");
     });
 }


export function callback_position(data_json){
    let mapa = window.maps.map;
     if (mapa){
         // clean all markers
         console.log(maps);
         console.log(mapa);
         console.log("Map modal", maps.map_modal);
         //let markers =  mapa.getMarkers();
         //window.clear_map("map", "ubicacion");
         // add new marker to
         let data =  data_json["ubicacion"]["from_profile"]["ubicacion"];
         console.log("Data position",data);
         put_marker(mapa, data);
         // move to
         // how to move
         // popup https://openstreetmap.be/en/projects/howto/openlayers.html
         // ref:
         //https://gitlab.com/pineiden/ol-map-network/-/blob/master/src/index.js
         move_to(mapa, data);
         // fill input field
         let lugar=document.getElementById('lugar');
         lugar.value = direccion;
     };
     /*closing modal-> updating field*/
    close_modal("modal_ubicacion");

 }


export function update_position(button){
    let field_names = ["ciudad",
                       "lugar", "pais",
                       "latitud", "longitud",
                        "x", "y"];
     // tomar los valores del formulario
     // solamente los campos nombrados arriba
     let form = document.getElementById('form-lugar');
    let values = {};
     field_names.forEach(name=>{
         values[name]=form.elements[name].value;
     });
     let fields = {"ubicacion":{
         name:"ubicacion",
         field_name:"ubicacion",
         value:values,
         type:"rtx"}};
     let is_file = false;
     let is_dom=false;
     update_field(window.username, fields, window.update_url, is_file, callback_position, is_dom);
 }


