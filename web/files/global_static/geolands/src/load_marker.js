import {
    Circle as CircleStyle,
    Fill,
    Icon,
    Stroke,
    Style,
} from 'ol/style';
// Estilos de marcador
// Read svg

import {svgMarker} from './svg_tpl';

export function loadMarkers() {
    //const path=new pwd();
    const iconSvgPath = __dirname+'/img/gnss.tpl.svg';

/*    console.log("read", iconSvgPath, fs);
    const svgMarker = fs.readFileSync(iconSvgPath, 'utf8');

    console.log("Marker svg", svgMarker);*/

  const markerColors = {
    rtx: '#0092e8',
    no_rtx: 'red',
    no_csn: 'orange',
  };

  const regex = '#0092e8';

  /* Insert in html the marker */

  const markers = {};

  Object.keys(markerColors).forEach((elem) => {
    const marker = svgMarker.replace(regex, markerColors[elem]);
    const dataSvg = `data:image/svg+xml;charset=utf-8,${encodeURIComponent(marker)}`;
    markers[elem] = dataSvg;
  });

  return markers;
}

export function loadIcons(markers) {
  const icons = {};

  Object.keys(markers).forEach((elem) => {
    const iconPosition = {
      src: markers[elem],
      imgSize: [363, 480],
      opacity: 1,
      scale: 0.06,
      anchor: [0.5, 1],
      anchorXUnits: 'fraction',
      anchorYUnits: 'fraction',
    };
    icons[elem] = new Icon(iconPosition);
  });

  return icons;
}

export function loadStyles(markers, icons) {
  const svgStyles = {};
  Object.keys(markers).forEach((elem) => {
    const svgStyle = new Style({ image: icons[elem] });
    svgStyles[elem] = svgStyle;
  });
  const iconPath = './img/gnss.png';
  const styles = {
    route: new Style({
      stroke: new Stroke({
        width: 6,
        color: [237, 212, 0, 0.8],
      }),
    }),
    icon: new Style({
      image: new Icon({
        anchor: [0.5, 1],
        src: iconPath,
      }),
    }),
    geoMarker: new Style({
      image: new CircleStyle({
        radius: 7,
        fill: new Fill({ color: 'black' }),
        stroke: new Stroke({
          color: 'white',
          width: 2,
        }),
      }),
    }),
    rtx: svgStyles.rtx,
    no_rtx: svgStyles.no_rtx,
    no_csn: svgStyles.no_csn,
  };
    return styles;
}
//
