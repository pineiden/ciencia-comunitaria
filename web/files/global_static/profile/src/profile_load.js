import {show_thumb_image} from './profile.js';
import {changeInteraction, load_maps,
        take_position, move_to, update_place,
        update_position} from '../../geolands/src/index.js';

document.addEventListener("DOMContentLoaded", function(event){
    // en file_input muestra al cambio
    const fileInputs = document.querySelectorAll('.file-input');
    fileInputs.forEach(fileInput=>{
        let name = fileInput.name;
        let element_id = name+"_show" ;
        fileInput.onchange = () => {
            if (fileInput.files.length > 0) {
                const fileName = document.querySelector("#"+name+"_name");
                fileName.textContent = fileInput.files[0].name;
                show_thumb_image(fileInput, element_id);
            }
        };
    });
    /**
     * onchange callback on the select element.
     */
    changeInteraction();
    load_maps();
    /*
      connect buttons
    */
    let button_update=document.getElementById('update_place_btn');
    button_update.onclick=function(){update_place('ubicacion');};

    // set taking data from modal
    let take_button = document.getElementById('take_position');
    if (take_button){
        take_button.addEventListener("click",
                                     function(){
                                         let
                                         tk=take_position('form-lugar','map_destiny');
                                         console.log("TK->",tk);
                                         move_to(tk.map, tk.features);
                                     });
    }


});

