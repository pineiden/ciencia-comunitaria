import {update_field} from '../../js/update_field.js';

function enable(row_id){
    row_id.readOnly=false;
};
function save_field(row_id){
    update_field(window.username, [row_id], window.update_url); 
};
function disable(row_id){
    row_id.readOnly=true;
};

function update_image(modal_id){
    let modal=document.getElementById("modal_"+modal_id);
    modal.classList.add("is-active");
    let modal_close=document.getElementById("modal_close_"+modal_id);
    modal_close.addEventListener("click", event=>{
        modal.classList.remove("is-active");
    });
}

function callback_image(data_json){
    let im = document.getElementById("img_perfil");
    im.setAttribute('src',data_json.image.url);

}

function save_image_profile(button){
    let tag = document.getElementById("tag_image");
    let file_profile = document.getElementById("image_file");
    let tag_name = tag.name;
    let image=file_profile.files[0];
    let fields = {"tag":tag, "image":file_profile};
    let is_file = true;
    let is_dom = true;
    update_field(window.username, fields, window.update_url,is_file, callback_image, is_dom);
}


export function show_thumb_image(input, element_id){
    if(input.files && input.files[0]){
        var reader = new FileReader();
        let file = input.files[0];
        reader.onload =function(e){
            let element = document.getElementById(element_id);
            let element_modal = document.getElementById(element_id+"_x_img");
            element.setAttribute('src', e.target.result);
            element.setAttribute('alt', file.name);
            if (element_modal){
                element_modal.setAttribute('src', e.target.result);
                element_modal.setAttribute('alt', file.name);
            }
        };
        //finally read the file from source
        reader.readAsDataURL(file);
    }
};
