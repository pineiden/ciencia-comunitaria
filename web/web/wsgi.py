"""
WSGI config for web project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

MODO = os.environ.get('MODO', 'dev')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', f'web.settings.{MODO}')

application = get_wsgi_application()
