"""web URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from apps.home.views import (page_not_found, bad_request, permission_denied,
                             server_error)

urlpatterns = [
    path('__debug__/', include(debug_toolbar.urls)),
    path('404/', page_not_found),
    path('403/', permission_denied),
    path('400/', bad_request),
    path('500/', server_error),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('filer/', include('filer.urls')),
    path('captcha/', include('captcha.urls'), name='captcha'),
    path('', include('apps.home.urls', namespace='home'), name='home'),
    path('pagina/',
         include('apps.pagina.urls', namespace='pagina'),
         name='pagina'),
    path('contacto/',
         include('apps.contacto.urls', namespace='contacto'),
         name='contacto'),
    path("admin_group/",
         include('apps.admin_group.urls', namespace='admin_group'),
         name='admin_group'),
    path('groups_manager/',
         include('groups_manager.urls', namespace='groups_manager'),
         name='groups_manager'),
    path('accounts/',
         include('apps.accounts.urls', namespace='accounts'),
         name='accounts'),
    path('proyecto/',
         include('apps.proyecto.urls', namespace='proyecto'),
         name='proyecto'),
    path('parametro/',
         include('apps.parametro.urls', namespace='parametro'),
         name='parametro'),
    path('martor/', include('martor.urls')),
]

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
