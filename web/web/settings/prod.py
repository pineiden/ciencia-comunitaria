import os
from .base import *
from .base import INSTALLED_APPS, BASE_DIR, MIDDLEWARE

DEBUG = False

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                "files.functions.context_settings.parameters"
            ],
            'builtins': [],
            'libraries': {
                'new_tags': 'files.tags.new_tags',
                'filter_numeric': 'files.filters.numeric'
            }
        },
    },
]

DEBUG = True

# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

ALLOWED_HOSTS = ['localhost', 'cursodeprogramacion.cl', '*']

STATIC_FILES = 'django.contrib.staticfiles'
if STATIC_FILES in INSTALLED_APPS:
    position = INSTALLED_APPS.index(STATIC_FILES)
    INSTALLED_APPS.insert(position, 'livereload')
    MIDDLEWARE.append('livereload.middleware.LiveReloadScript')

ENGINE = os.environ.get('ENGINE', 'sqlite3')

if ENGINE == 'sqlite3':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': BASE_DIR / 'db.sqlite3',
        }
    }
elif ENGINE == 'postgis':
    DBNAME = os.environ.get('DBNAME')
    DBUSER = os.environ.get('DBUSER')
    DBPASS = os.environ.get('DBPASS')
    DBHOST = os.environ.get('DBHOST', 'localhost')
    DBPORT = os.environ.get('DBPORT', 5432)
    engine = 'django.contrib.gis.db.backends.postgis'
    DATABASES = {
        'default': {
            'ENGINE': engine,
            'NAME': DBNAME,
            'USER': DBUSER,
            'PASSWORD': DBPASS,
            'HOST': DBPORT,
            'PORT': DBHOST,
        },
    }

SITE_URL = "www.cienciacomunitaria.cl"
SITE_NAME = "Ciencia Comunitaria"

SITE_ID = 1
SECURE_SSL_REDIRECT = True
CSRF_COOKIE_HTTPONLY = True

ADMIN_EMAILS = [
    'dpineda@uchile.cl',
]

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOWED_ORIGINS = [
    f"https://{SITE_URL}",
    "https://cienciacomunitaria.cl",
]

SITE_URL = 'localhost:8000'
