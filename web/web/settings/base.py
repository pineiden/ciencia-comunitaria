"""
Django settings for web project.

Generated by 'django-admin startproject' using Django 3.1.2.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.1/ref/settings/
"""

from .names import *
from pathlib import Path
from .installed import INSTALLED_APPS
from .menu import MENU_NAV, CONTACT_EMAIL
from .filer import *

import os
# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = str(Path(__file__).resolve().parent.parent.parent)
FILES_DIR = Path(f'{BASE_DIR}/files')
PROJECTS_DIR = Path(f'{BASE_DIR}/apps')
STATIC_ROOT = Path(f"{BASE_DIR}/static")

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!

ALLOWED_HOSTS: list = []

# Application definition

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

ROOT_URLCONF = 'web.urls'

WSGI_APPLICATION = 'web.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'es'

TIME_ZONE = 'America/Santiago'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATICFILES_DIRS = [
    FILES_DIR / 'global_static/',
    PROJECTS_DIR / "home/static/",
    PROJECTS_DIR / "admin_group/static/",
    PROJECTS_DIR / "contacto/static/",
    PROJECTS_DIR / "pagina/static/",
]

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

MEDIA_ROOT = FILES_DIR / "media"
MEDIA_URL = '/media/'

STATIC_URL = '/static/'

FILE_UPLOAD_PERMISSIONS = 0o644

# celery conf
CELERY_RESULT_BACKEND = 'django-db'
CELERY_CACHE_BACKEND = 'django-cache'
CELERY_BROKER_URL = 'redis://localhost:6379'
CELERY_RESULT_BACKEND = 'redis://localhost:6379'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = TIME_ZONE
# For django guardian (permissions)
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    "django.contrib.auth.backends.RemoteUserBackend",
    'guardian.backends.ObjectPermissionBackend',
)
# groups manager
GROUPS_MANAGER = {
    "GROUP_NAME_PREFIX": "",
    "USER_USERNAME_PREFIX": "",
    'AUTH_MODELS_SYNC': False,
    'AUTH_MODELS_GET_OR_CREATE': True,
    'GROUP_NAME_SUFFIX': '',
    'USER_USERNAME_SUFFIX': '',
    'TEMPLATE_STYLE': 'bulma',
    'PERMISSIONS': {
        'owner': ['view', 'change', 'delete'],
        'group': ['view', 'change'],
        'groups_upstream': ['view'],
        'groups_downstream': [],
        'groups_siblings': ['view'],
    },
}

CAPTCHA_FONT_SIZE = 44
CAPTCHA_IMAGE_SIZE = (300, 160)

# LOGIN

LOGIN_URL = '/cuentas/login'
LOGOUT_REDIRECT_URL = "/"
DATA_UPLOAD_MAX_MEMORY_SIZE = 8388608
